Vitalnix User Management Suite
==============================

This is a collection of programs for managing Linux users and groups in a
multitude of databases. Using a plugin architecture, different storage methods
can be interfaced with; currently there is support for Shadow, LDAP and MySQL.

The suite comes with useradd(8)-like command-line programs for single
operations, programs for synchronization against another data source such as
plain text files, web interface scripts, login restriction plugins, and print
accounting.

Vitalnix provides a library, _libvxdb_, which encapsulates away the underlying
storage mechanism and provides generalized methods of basic operations such as
“add a user”, modify, delete. With regard to user/group information retrieval,
it is mostly equivalent to NSS, but is thread-safe and multi-use safe.

Vitalnix is not the solution to everything. It does not allow you to add
arbitrary attributes to a user account, even if the underlying storage
mechanisms could be adopted to do so.

Of course, there is a bit more than just libvxdb. SAMBA Logon Time Restriction
and Print Accounting are two parts for example, which are not directly related
to user management, but they were nonetheless needed.
